CC=gcc
COMPILATION_FLAGS=-Wall -Wextra -pedantic -O3 -ansi
DEBUG_FLAGS=-g
BIN=bin/
SRC=src
VPATH=$(SRC)
EXEC=vertex_cover

.PHONY:
	mrproper clean default doc all

default: ${EXEC}

${BIN}utils.o: utils.c utils.h

${BIN}adjarray.o: adjarray.c adjarray.h utils.h

${BIN}triangles.o: triangles.c triangles.h adjarray.h

${BIN}priority_queue.o: priority_queue.c priority_queue.h utils.h adjarray.h

${BIN}vertex_cover.o: vertex_cover.c vertex_cover.h adjarray.h triangles.h utils.h

${BIN}main.o: main.c adjarray.h utils.h triangles.h vertex_cover.h

${EXEC}: ${BIN}main.o ${BIN}utils.o ${BIN}adjarray.o ${BIN}triangles.o ${BIN}priority_queue.o ${BIN}vertex_cover.o
	${CC} -o $@ $^ ${COMPILATION_FLAGS} ${DEBUG_FLAGS}

doc:
	doxygen doxygen_config

all: ${EXEC} doc

clean:
	rm -f ${BIN}*.o

mrproper: clean
	rm -f ${EXEC}
	rm -rf doc/

$(BIN)%.o : %.c
	$(CC) -c $< $(COMPILATION_FLAGS) ${DEBUG_FLAGS} -o $@

