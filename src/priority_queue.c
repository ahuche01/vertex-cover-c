#include <stdio.h>

#include "adjarray.h"
#include "utils.h"
#include "priority_queue.h"

node_degree_list* initialize_priority_queue(adjlist* g) {
	node_degree_list* deg_l = malloc_wrapper(sizeof(node_degree_list));;
	deg_l->degree_list =
		malloc_wrapper((g->number_nodes+1)*sizeof(node_degree));
	deg_l->length = 0;
	return deg_l;
}

void free_degree_list(node_degree_list* deg_l) {
	free(deg_l->degree_list);
	free(deg_l);
}

node_degree empty_node_degree() {
	node_degree nd;
	nd.node = 0;
	nd.degree = 0;
	return nd;
}

node_degree get_left_child(node_degree_list* deg_l, unsigned long i) {
	if (2*(i+1)-1 < deg_l->length) {
		return deg_l->degree_list[2*(i+1)-1];
	}
	return empty_node_degree();
}

node_degree get_right_child(node_degree_list* deg_l, unsigned long i) {
	if (2*i+1 < deg_l->length) {
		return deg_l->degree_list[2*(i+1)];
	}
	return empty_node_degree();
}

void swap(node_degree_list* deg_l, unsigned long i, unsigned long j) {
	node_degree nd;
	nd = deg_l->degree_list[i];
	deg_l->degree_list[i] = deg_l->degree_list[j];
	deg_l->degree_list[j] = nd;
}

void swap_parent(node_degree_list* deg_l, unsigned long i) {
	swap(deg_l, i, (i-1)/2);
}

node_degree get_parent(node_degree_list* deg_l, unsigned long i) {
	return deg_l->degree_list[(i-1)/2];
}

char has_parent(unsigned long i) {
	return i > 0;
}

void balance(node_degree_list* deg_l, unsigned long i) {
	while (has_parent(i)
			&& i != 0
			&& deg_l->degree_list[i].degree < get_parent(deg_l, i).degree) {
		swap_parent(deg_l, i);
		if (has_parent(i)) {
			balance(deg_l, (i-1)/2);
		}
	}
}

char check_priority_queue_consistency(node_degree_list* deg_l) {
	unsigned long i;
	for (i = 1; i < deg_l->length; i++) {
		if (deg_l->degree_list[i].degree < get_parent(deg_l, i).degree) {
			printf("%lu (node %lu, index %lu) is after %lu (node %lu, index "
					"%lu)\n", deg_l->degree_list[i].degree,
					deg_l->degree_list[i].node, i, get_parent(deg_l, i).degree,
					get_parent(deg_l, i).node, (i-1)/2);
			return 0;
		}
	}
	return 1;
}

void add_node_pqueue(unsigned long node, unsigned long degree,
					node_degree_list* deg_l) {
	node_degree nd;
	nd.node = node;
	nd.degree = degree;
	deg_l->degree_list[deg_l->length] = nd;
	balance(deg_l, deg_l->length++);
}

void populate_priority_queue(adjlist* g, node_degree_list* deg_list) {
	unsigned long i;
	for (i = 0; i < g->number_nodes+1; i++) {
		unsigned long j;
		if ((j = get_degree(i,g))) {
			add_node_pqueue(i, j, deg_list);
		}
	}
}

void print_priority_queue(node_degree_list* deg_l) {
	unsigned long i;
	for (i = 0; i < deg_l->length; i++) {
		printf("Degree %lu, node %lu\n", deg_l->degree_list[i].degree,
				deg_l->degree_list[i].node);
	}
}

void balance_2(node_degree_list* deg_l, unsigned long i) {
	while (2*i+1 < deg_l->length && deg_l->length > 1) {
		unsigned long min_child;
		/* printf("Current node %lu %lu %lu\n", deg_l->degree_list[i].degree,
					deg_l->degree_list[i].node, i); */
		/* printf("Left child %lu, right child %lu\n",
					get_right_child(deg_l, i).degree,
					get_left_child(deg_l, i).degree); */
		if (deg_l->degree_list[i].degree > get_right_child(deg_l, i).degree
				|| deg_l->degree_list[i].degree >
					get_left_child(deg_l, i).degree) {
			if (get_right_child(deg_l, i).degree <
					get_left_child(deg_l, i).degree) {
				/* printf("Choosing right child\n"); */
				min_child = 2*(i+1);
			} else {
				min_child = 2*(i+1)-1;
		}
		/* printf("Swapping node %lu (deg %lu index %lu) with node %lu (deg "
					"%lu index %lu)\n", deg_l->degree_list[i].node,
					deg_l->degree_list[i].degree, i,
					deg_l->degree_list[min_child].node,
					deg_l->degree_list[min_child].degree, min_child); */
		swap(deg_l, i, min_child);
		i = min_child;
		/*print_priority_queue(deg_l);*/
		} else {
			/* printf("Smaller than both children\n"); */
			return;
		}
	}
}

node_degree get_min_element(node_degree_list* deg_l) {
	if (deg_l->length) {
	node_degree nd = deg_l->degree_list[0];
	/* printf("Removing node\n"); */
	deg_l->degree_list[0] = deg_l->degree_list[deg_l->length-1];
	deg_l->length--;
	deg_l->degree_list[deg_l->length] = empty_node_degree();
	/* printf("Swapped, now balancing\n"); */
	balance_2(deg_l, 0);
	return nd;
	} else {
		return empty_node_degree();
	}
}

