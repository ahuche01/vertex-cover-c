#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

void* fopen_wrapper(char *path, char *mode) {
	FILE *file;
	if (NULL == (file = fopen(path, mode))) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	return file;
}

void* malloc_wrapper(size_t size) {
	void *tmp;
	if (NULL == (tmp = malloc(size))) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	return tmp;
}

void* calloc_wrapper(size_t number_items, size_t size) {
	/* calloc also initializes all blocks to 0 */
	void *tmp;
	if (NULL == (tmp = calloc(number_items, size))) {
		perror("calloc");
		exit(EXIT_FAILURE);
	}
	return tmp;
}

void* realloc_wrapper(void* pointer, size_t number_items, size_t size) {
	void *tmp;
	if (NULL == (tmp = realloc(pointer, number_items * size))) {
		printf("realloc null pointer.\n");
		perror("realloc");
		exit(EXIT_FAILURE);
	}
	return tmp;
}

unsigned long max3(unsigned long a, unsigned long b, unsigned long c) {
	return a > b ? (a > c ? a : c) : (b > c ? b : c);
}

Error_enum strtoul_wrapper(char *string, unsigned long *value) {
	char *error;
	if (!string)
		return INVAL_ERROR;
	errno = 0;
	*value = strtoul(string, &error, 10);
	if (error[0] != '\0' || errno != 0)
		return INVAL_ERROR;
	return NO_ERROR;
}

