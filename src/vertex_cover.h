/**
 * @file vertex_cover.h
 */

#ifndef __VERTEX_COVER__
#define __VERTEX_COVER__

#include "adjarray.h"

/**
 * Adds a node to a list of nodes
 *
 * @param vertex_cover The list of nodes
 * @param node The node to add
 *
 * @return void
 */
void add_node_vc(node_list* vertex_cover, unsigned long node);

/**
 * Adds a list of nodes to another list of nodes
 *
 * @param vertex_cover The list that will receive nodes
 * @param nodes The nodes that will be added to the other list
 *
 * @return void
 */
void add_node_list_vc(node_list* vertex_cover, node_list* nodes);

/**
 * Phase 1 of the vertex cover planar approximation algorithm:
 * - Gets disjoint triangles (naive) from the graph g
 * - Adds them all to the vertex cover
 * - Removes them all from the graph
 *
 * @param g The graph we are working on
 *
 * @return A list of nodes corresponding to the start of a vertex cover
 */
node_list* phase_1(adjlist* g);

/**
 * Phase 2 of the vertex cover planar approximation algorithm:
 *
 * @param vertex_cover A vertex cover as given by the phase_1 function
 * @param g The graph we are working on
 *
 * @return A list of nodes corresponding to the final approximation of the
 * vertex cover.
 */
node_list* phase_2(node_list* vertex_cover, adjlist* g);

/**
 * Approximation algorithm for the vertex cover problem for planar graphs.
 *
 * @param g The graph we are working on
 *
 * @return A vertex cover
 */
node_list* vertex_cover_planar_approximation(adjlist* g);

/**
 * Checks the vertex cover provided covers all the edges of the graph g
 *
 * @param vertex_cover A list of nodes representing the vertex cover
 * @param g The graph we are working on
 *
 * @return 1 if all edges are covered, 0 otherwise
 */
Error_enum check_correctness(node_list* vertex_cover, adjlist* g);

#endif

