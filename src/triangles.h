/**
 * @file triangles.h
 *
 * This module contains functions to manage triangles in a graph
 */

#ifndef __TRIANGLES__
#define __TRIANGLES__

#include "adjarray.h"

/** A structure representing a triangle */
typedef struct {
	/** First vertex of the triangle */
	unsigned long a;
	/** Second vertex of the triangle */
	unsigned long b;
	/** Thirs vertex of the triangle */
	unsigned long c;
} triangle;

/** Structure containing a list of triangles */
typedef struct {
	/** The length of the array of triangles */
	unsigned long length;
	/** Array of triangles */
	triangle *triangles;
} triangles_list;

/**
 * Checks if tmp_triangle is disjoint from all other triangles in t_list. That
 * is, it has no common node with any of the triangles of t_list.
 *
 * @param t_list The list of triangles
 * @param tmp_triangle The triangle to be checked
 *
 * @return 1 if disjoint, 0 if not
 */
char disjoint_triangle(triangles_list *t_list, triangle *tmp_triangle);

/**
 * Add the triangle tmp_triangle to t_list only if it is disjoint from all
 * other triangles in t_list.
 *
 * @param t_list The list of triangles
 * @param tmp_triangle The triangle to be added to the list
 *
 * @return void
 */
void add_disjoint_triangle(triangles_list *t_list, triangle* tmp_triangle);

/**
 * Frees triangles_list data structure.
 *
 * @param t_list The list of triangles to be freed
 *
 * @return void
 */
void free_triangles_list(triangles_list *t_list);

/**
 * Naive algorithm to find triangles: checks for each edge if both its ends
 * have a common neighbour.
 *
 * @param g The graph we are working on
 *
 * @return a list of triangles
 */
triangles_list* find_triangles_naive(adjlist *g);

/**
 * Prints a triangle
 *
 * @return void
 */
void print_triangle(triangle t);

/**
 * Prints a list of triangles
 *
 * @return void
 */
void print_all_triangles(triangles_list *t);

/**
 * Naive and non generic implementation to find triangles: checks for each edge
 * if both its ends have a common neighbour.
 *
 * @param g The graph we are working on
 *
 * @return a list of triangles
 */
triangles_list* find_triangles_naive_adjlist(adjlist *g);

#endif

