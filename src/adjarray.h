/**
 * @file adjarray.h
 *
 * This modules contains functions to parse an input file and build an
 * adjacency list.
 *
 * Precondition:
 * - The file must be formatted like vertex space vertex or vertex tab vertex.
 * - Comments must span a whole line and start with a '#'
 * - A node of degree 0 doesn't exist
 */

#ifndef __READ__
#define __READ__

#include "utils.h"

/** 10 000 * sizeof(unsigned long) = 80 KB. Might need to increase it for
 * bigger graphs.
 */
#define NBEDGES 10000

/** Structure containing two nodes representing an edge. */
typedef struct {
	/** The source node of an edge */
	unsigned long s;
	/** The target node of an edge */
	unsigned long t;
} edge;

/**
 * Structure representing a graph, given as an adjacency list, represented as
 * arrays.
 */
typedef struct {
	/** Array of edges of the graph */
	edge *edges;
	/** Number of edges in the graph */
	unsigned long number_edges;
	/** The highest id of a node as specified in the input file */
	unsigned long number_nodes;
	/** cumulative_degrees is an array of indices specifying where the
	* neighbours of a given node are located in the array neighbours.
	* cumulative_degree[i] = j, cumulative_degree[i+1] = k means that the
	* neighbours of node i start at neighbours[j] and end at negighbours[k]
	* excluded. */
	unsigned long *cumulative_degrees;
	/** Array of neighbours. Each node has an interval given by the array
	* cumulative_degrees. Every index within that interval corresponds to a
	* neighbour in the array neighbours. */
	unsigned long *neighbours ;
} adjlist;

/**
 * Structure containing two indices: The first and last neighbour of a given
 * node in the array neighbours
 */
typedef struct {
	/** Starting index in neighbours array */
	unsigned long from;
	/** Last index in neighbours array */
	unsigned long to;
} neighbours_id;

/**
 * List of neighbours_id, used to get the neighbours of neighbours of a node.
 */
typedef struct {
	/** List of neighbours_id */
	neighbours_id* n_list;
	/** Number of element in the neighbours_id list */
	unsigned long length;
} neighbours_id_list;

/**
 * List of nodes
 */
typedef struct {
	/** List of integers, representing nodes */
	unsigned long* list;
	/** Number of elements in the list */
	unsigned long length;
} node_list;

/**
 * Parses an input string into two unsigned long
 *
 * @param string The string to parse
 * @param s
 * @param t
 *
 * @return An error id if it couldn't parse or if the line was a comment
 */
Error_enum read_line(char* string, unsigned long* s, unsigned long* t);

/**
 * Read an edge list from the file file_name and populates the array edges of
 * and adjlist.
 *
 * @return An adjlist with populated edges array
 */
adjlist* readEdgeList(char* file_name);

/**
 * Builds an adjlist given an edge list
 *
 * @param g The graph we are working on
 *
 * @return void
 */
void makeadjlist(adjlist* g);

/**
 * Frees and adjlist
 *
 * @param g The adjlist to be freed
 *
 * @return void
 */
void free_adjlist(adjlist *g);

/**
 * Neighbours of node are between neighbours[node] up to (not including)
 * neighbours[node+1]. Returns the difference of those bounds.
 *
 * @param node We want the degree of that node
 * @param g The graph we are working on
 *
 * @return The degree of node
 */
unsigned long get_degree(unsigned long node, adjlist* g);

/**
 * Prints the degree of node in g
 *
 * @param node The node we are working on
 * @param g The graph we are working on
 *
 * @return void
 */
void print_degree(unsigned long node, adjlist* g);

/**
 * Prints the degree of all nodes in the graph
 *
 * @param g The graph we are working on
 *
 * @return void
 */
void print_all_degrees(adjlist* g);

/**
 * Prints the neighbours of node in g
 *
 * @param node The node we are working on
 * @param g The graph we are working on
 *
 * @return void
 */
void print_neighbours(unsigned node, adjlist* g);

/**
 * Prints the neighbours of all nodes in the graph g
 *
 * @param g The graph we are wokring on
 *
 * @return void
 */
void print_all_neighbours(adjlist* g);

/**
 * Checks if node have target as neighbour
 *
 * @param node The source node
 * @param target The target node
 * @param k A padding that is used when removing a node. The upper bound given
 * by the cumulative_degrees array is temporarily k too big
 * @param g The graph on which we are working
 *
 * @return The id of the neighbour or (unsigned long)-1 if node doesn't have
 * target as neighbour (I would have chosen 0 but what if the target is at
 * g->neighbours[0] already?)
 */
unsigned long has_neighbour_k(unsigned long node, unsigned long target,
								unsigned long k, adjlist* g);

/**
 * Fetches the bounds of each neighbour of a given node. The bounds specify the
 * starting and end index of the neighbours of the given node in the neighbours
 * array of the adjacency array.
 *
 * @param node The given node
 * @param g The graph we are working on
 *
 * @return A list of bounds (neighbours_id list)
 */
neighbours_id_list* get_neighbours_2_bounds(unsigned long node, adjlist* g);

/**
 * Frees a list of bounds of neighbours
 *
 * @param neigh_list The list to be freed
 *
 * @return void
 */
void free_neighbours_2(neighbours_id_list* neigh_list);

/**
 * Prints the neighbours of a node, given its bounds
 *
 * @param nid The bounds of the node
 * @param g The graph we are working on
 *
 * @return void
 */
void print_neighbours_2_(neighbours_id* nid, adjlist* g);

/**
 * Checks if a given node is not in a given node_list
 *
 * @param node The node we are looking for
 * @param node_list The list were we will be looking
 *
 * @return 1 if not found, 0 if found
 */
char notin(unsigned long node, node_list* node_list);

/**
 * Checks that a target node isn't the neighbour of a given node in a graph g
 *
 * @param target A target node we need to make sur isn't the neighbour
 * @param node The node we will analyze the neighbours of
 * @param g The graph we are working on
 *
 * @return 1 if target is the neighbours of node in g, 0 otherwise
 */
char not_neighbour(unsigned long target, unsigned long node, adjlist* g);

/**
 * Builds a list of nodes that are at distance 2 of a given node in a graph g
 *
 * @param n_list A list of bounds on nodes
 * @param g The graph we are working on
 * @param node The starting node
 *
 * @return A list of nodes
 */
node_list* build_neigh_2_list(neighbours_id_list* n_list, adjlist* g, unsigned long node);

/**
 * Frees a list of nodes
 *
 * @param node_list The list of nodes to be freed
 *
 * @return void
 */
void free_node_list(node_list* node_list);

/**
 * Prints a list of nodes
 *
 * @param res The list of nodes to be printed
 *
 * @return void
 */
void print_neighbours_2_list(node_list* res);

/**
 * Build a list of nodes that are at distance 2 of a given node in a graph g
 *
 * @param node A given node
 * @param g The graph we are working on
 *
 * @return A list of nodes
 */
node_list* get_neighbours_2_list(unsigned long node, adjlist* g);

/**
 * Prints a list of nodes given their bounds
 *
 * @param neigh_list The list of bounds
 * @param g The graph we are working on
 *
 * @return void
 */
void print_neighbours_2(neighbours_id_list* neigh_list, adjlist* g);

/**
 * Checks if a given node is not in a given graph
 *
 * @param node The node we want to check
 * @param g The graph we are working on
 *
 * @return 1 if node not in the graph g, 0 otherwise
 */
char not_in_graph(unsigned long node, adjlist* g);

/**
 * Checks if a given node in in a given graph
 *
 * @param node The node we want to check
 * @param g The graph we are working on
 *
 * @return 0 if node not in the graph g, 1 otherwise
 */
char in_graph(unsigned long node, adjlist* g);

/**
 * Updates the cumulative_degrees array following the deletion of node
 *
 * @param node The node that is being removed
 * @param degree The degree of the node before its deletion
 * @param g The graph we are working on
 *
 * @return void
 */
void update_cumulative_degrees(unsigned long node, unsigned long degree,
		adjlist* g);

/**
 * Updates the neighbours array following the deletion of node
 *
 * @param node The node that is being removed
 * @param degree The degree of the node being removed
 * @param cumulative_degrees_node The index of the array neighbours at which
 * the neighbours of node start
 * @param g The graph we are working on
 *
 * @return void
 */
void update_neighbours(unsigned long node, unsigned long degree,
		unsigned long cumulative_degrees_node, adjlist* g);

/**
 * Updates the edge array following the deletion of node
 *
 * @param node The node that is being deleted
 * @param g The graph we are working on
 *
 * @return void
 */
void update_edges(unsigned long node, adjlist* g);

/**
 * Updates the number of node following the deletion of a node
 *
 * @param g The graph we are working on
 *
 * @return void
 */
void update_number_nodes(adjlist* g);

/**
 * Reallocates the edges array following the deletion of a node
 *
 * @param g The graph we are working on
 *
 * @return void
 */
void realloc_edges(adjlist* g);

/**
 * Reallocates the neighbours array following the deletion of a node
 *
 * @param g The graph we are working on
 *
 * @return void
 */
void realloc_neighbours(adjlist* g);

/**
 * Reallocates the cumulative_degrees array following the deletion of a node
 *
 * @param g The graph we are working on
 *
 * @return void
 */
void realloc_cumulative_degrees(adjlist* g);

/**
 * Removes a node. A node of degree 0 doesn't exist
 *
 * @param node The node to be removed
 * @param g The graph from which the node will be removed
 *
 * @return void
 */
void rm_node(unsigned long node, adjlist* g);

/**
 * Returns the number of edges in the graph
 *
 * @param g The graph we are working on
 *
 * @return the number of edges in the graph
 */
unsigned long get_number_edges(adjlist* g);

/**
 * Returns the number of nodes in the graph. In this case, this is the node of
 * highest id.
 *
 * @param g The graph we are working on
 *
 * @return the max id over all nodes of the graph
 */
unsigned long get_number_nodes(adjlist* g);

/**
 * Gets the ith neighbour of node in the graph g
 *
 * @param node The node we are working on
 * @param i The neighbour we want to get
 * @param g The graph we are workgin on
 *
 * @return The ith neighbour of node in g
 */
unsigned long get_neighbour(unsigned long node, unsigned long i, adjlist* g);

/**
 * Gets the ith edge of the graph g
 *
 * @param i The id of the edge we need
 * @param g The graph we are working on
 *
 * @return A struct edge
 */
edge get_edge(unsigned long i, adjlist* g);

#endif

