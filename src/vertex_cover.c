#include <stdio.h>

#include "adjarray.h"
#include "triangles.h"
#include "utils.h"
#include "priority_queue.h"

void add_node_vc(node_list* vertex_cover, unsigned long node) {
	vertex_cover->list[vertex_cover->length++] = node;
}

void add_node_list_vc(node_list* vertex_cover, node_list* nodes) {
	unsigned i;
	for (i = 0; i < nodes->length; i++) {
		vertex_cover->list[vertex_cover->length++] = nodes->list[i];
	}
}

node_list* phase_1(adjlist* g) {
	node_list *vertex_cover = malloc_wrapper(sizeof(node_list));
	triangles_list *t = find_triangles_naive(g);
	unsigned long i;

	/* A vertex cover won't have more vertices than the number of nodes in the
	* graph */
	vertex_cover->list = malloc_wrapper(get_number_nodes(g)*sizeof(unsigned long));
	vertex_cover->length = 0;

	/* For each triangle */
	for (i = 0; i < t->length; i++) {
		/* Add each node of the triangle to the vertex cover */
		add_node_vc(vertex_cover, t->triangles[i].a);
		add_node_vc(vertex_cover, t->triangles[i].b);
		add_node_vc(vertex_cover, t->triangles[i].c);

		/* Remove each node of the triangle to the vertex cover */
		/*if (not_in_graph(t->triangles[i].a, g)) {printf("88888\n");}*/
		rm_node(t->triangles[i].a, g);
		/*if (not_in_graph(t->triangles[i].b, g)) {printf("66666\n");}*/
		rm_node(t->triangles[i].b, g);
		/* In case the triangle was disjoint from the graph, the last is
		* already removed */
		if (in_graph(t->triangles[i].c, g)) {
			rm_node(t->triangles[i].c, g);
		}
		printf("%lu out of %lu triangles processed\n", i, t->length-1);
	}
	free_triangles_list(t);
	return vertex_cover;
}

node_list* phase_2(node_list* vertex_cover, adjlist* g) {
	unsigned long iterations = 0;
	node_degree_list* ndl = initialize_priority_queue(g);
	populate_priority_queue(g, ndl);

	while (get_number_nodes(g)) {
		/* Pops the minimum element from the priority queue */
		node_degree nd;
		unsigned long nid_from;
		node_list *nneighbours;

		/* Since a node of degree 0 is counted as removed, we may need to pop a
		* few elements from the priority queue */
		do {
			nd = get_min_element(ndl);
		} while (not_in_graph(nd.node, g));
		/* Gets its neighbours */
		nid_from = g->cumulative_degrees[nd.node];
		/* Gets the nodes at distance 2 */
		nneighbours = get_neighbours_2_list(nd.node, g);

		/* For each neighbour, except the last one */
		while (get_degree(nd.node, g) > 1) {
			char matched = 0;
			unsigned long j;

			/* Add the current neighbour to the vertex cover */
			add_node_vc(vertex_cover, g->neighbours[nid_from]);

			/* For each node at distance 2 */
			for (j = 0; j < nneighbours->length; j++) {
				/* If there is an edge between the current neighbour and the
				* current node at distance 2 */
				if (has_neighbour_k(g->neighbours[nid_from],
									nneighbours->list[j], 0, g)
							< (unsigned long)-1
						&& !matched) {

					/* Add the node at distance 2 to the vertex cover, then
					* remove it from the graph */
					add_node_vc(vertex_cover, nneighbours->list[j]);
					if (not_in_graph(nneighbours->list[j], g)) {
						printf("Trying to remove node %lu, degree %lu, which "
								"isn't in the graph. Number of nodes: %lu\n",
								nneighbours->list[j],
								get_degree(nneighbours->list[j], g),
								get_number_nodes(g));
						printf("Is there an edge between %lu and %lu: %lu",
								g->neighbours[nid_from], nneighbours->list[j],
								has_neighbour_k(g->neighbours[nid_from],
												nneighbours->list[j], 0, g));
					}
					rm_node(nneighbours->list[j], g);
					/* Bounds might change after removing a node. Need to free
					* because get_neighbours allocates new memory. */
					nid_from = g->cumulative_degrees[nd.node];
					/* So that a neighbours doesn't get matched up with all its
					* neighbours */
					matched = 1;
				}
			}
			/* Remove the current neighbour from the graph */
			rm_node(g->neighbours[nid_from], g);
			/* Bounds might change after removing a node */
			nid_from = g->cumulative_degrees[nd.node];
		}
		/* Adding the last neighbour of v to the vertex cover and removing it
		* from the graph. */
		add_node_vc(vertex_cover, g->neighbours[nid_from]);
		rm_node(g->neighbours[nid_from], g);
		/* v doesn't need to be in the vertex cover since its neighbours
		* are. */

		/* The last node of the graph is removed when removed the second to
		* last node of the graph since a node of degree zero doesn't exist */
		if (in_graph(nd.node, g)) {
			/* Since all the neighbours of the current node should be removed,
			* the current node should have degree 0, thus be already removed
			* from the graph */
			rm_node(nd.node, g);
		}

		free_node_list(nneighbours);
		if (++iterations % 100 == 0) {
			printf("%lu iterations so far, %lu edges remaining\n", iterations,
					get_number_edges(g));
		}
	}
	free_degree_list(ndl);
	return vertex_cover;
}

node_list* vertex_cover_planar_approximation(adjlist* g) {
	return phase_2(phase_1(g), g);
}

Error_enum check_correctness(node_list* vertex_cover, adjlist* g) {
	char incorrect = 0;
	unsigned long i;
	for (i = 0; i < get_number_edges(g); i++) {
		if (notin(g->edges[i].s, vertex_cover)
				&& notin(g->edges[i].t, vertex_cover)) {
			printf("The edge (%lu,%lu) isn't covered\n", g->edges[i].s,
					g->edges[i].t);
			incorrect = 1;
		}
	}
	if (incorrect) {
		return ERROR;
	} else {
		return NO_ERROR;
	}
}

