#include <time.h>
#include <stdio.h>

#include "adjarray.h"
#include "utils.h"
#include "triangles.h"
#include "priority_queue.h"
#include "vertex_cover.h"

int main (int argc, char** argv) {
	time_t t1, t2;
	adjlist *g;
	/*
	triangles_list *t;
	unsigned i;
	node_degree_list* ndl;
	node_degree nd;
	*/
	node_list *vertex_cover;

	if (argc < 2) {
		fprintf(stderr, "Please specify a file.\n");
		return EXIT_FAILURE;
	}

	printf("Reading from file %s…\n", argv[1]);
	t1 = time(NULL);
	g = readEdgeList(argv[1]);

	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);

	printf("Building adjacency list…\n");

	makeadjlist(g);

	t2 = time(NULL);
	printf("Total time: %ldh%ldmn%lds\n", (t2-t1)/3600,
											(t2-t1)/3600/60,
											(t2-t1)%60);

	/*
	ndl = initialize_priority_queue(g);
	populate_priority_queue(g, ndl);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	nd = get_min_element(ndl);
	printf("Minimum element: node %lu, degree %lu\n", nd.node, nd.degree);
	printf("Consistent? %i\n", check_priority_queue_consistency(ndl));
	free_degree_list(ndl);
	*/

	/*
	t = find_triangles_naive_adjlist(g);
	t = find_triangles_naive(g);
	print_all_triangles(t);
	free_triangles_list(t);
	*/

	/*
	print_all_neighbours(g);
	rm_node(2, g);
	printf("Node 2 removed ------------------------------\n");
	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);
	print_all_neighbours(g);
	rm_node(3, g);
	printf("Node 3 removed ------------------------------\n");
	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);
	print_all_neighbours(g);
	rm_node(1, g);
	printf("Node 1 removed ------------------------------\n");
	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);
	print_all_neighbours(g);
	rm_node(4, g);
	printf("Node 4 removed ------------------------------\n");
	print_all_neighbours(g);
	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);
	rm_node(10, g);
	printf("Node 10 removed ------------------------------\n");
	print_all_neighbours(g);
	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);
	rm_node(8, g);
	printf("Node 5 removed ------------------------------\n");
	print_all_neighbours(g);
	printf("Number of nodes: %lu\n", g->number_nodes);
	printf("Number of edges: %lu\n", g->number_edges);
	*/

	/*
	for (i = 0; i < g->number_edges*2; i++) {
		printf("g->neighbours[%d] = %lu \n", i, g->neighbours[i]);
	}
	for (i = 1; i < g->number_nodes+2; i++) {
		printf("g->cumulative_degrees[%d] = %lu \n", i,
				g->cumulative_degrees[i]);
	}

	for (i = 0; i < g->number_edges; i++) {
		printf("g->edges[%d] = %lu, %lu\n", i, g->edges[i].s, g->edges[i].t);
	}
	print_all_neighbours(g);
	print_all_degrees(g);
	*/

	/*
	res = get_neighbours_2_list(1, g);
	print_neighbours_2_list(res);
	free_node_list(res);
	*/

	vertex_cover = vertex_cover_planar_approximation(g);
	printf("Nodes in the vertex cover:\n");
	print_neighbours_2_list(vertex_cover);
	printf("Size of the vertex cover: %lu\n", vertex_cover->length);

	/* Reparse the graph so I can check correctness */
	free_adjlist(g);
	g = readEdgeList(argv[1]);
	makeadjlist(g);
	if (check_correctness(vertex_cover, g)) {
		printf("The given vertex cover isn't correct\n");
	} else {
		printf("The given vertex cover is correct\n");
	}
	free_node_list(vertex_cover);

	free_adjlist(g);
	return EXIT_SUCCESS;
}

