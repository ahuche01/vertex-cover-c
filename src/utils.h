/**
 * @file utils.h
 *
 * This module contains wrappers for some standard C library functions along
 * with other utility functions.
 */

#ifndef __UTILS__
#define __UTILS__

#include <stdlib.h>

/** Enumerator of errors*/
typedef enum {
	/** No error */
	NO_ERROR,
	/** Generic error */
	ERROR,
	/** The given file was incorrectly formatted */
	FILE_FORMAT_ERROR,
	/** The given word was invalid, couldn't be turned into an integer */
	INVAL_ERROR,
	/** The given line was a comment thus not parsed */
	COMMENT
} Error_enum;

/**
 * fopen wrapper
 *
 * @param path The path to the file
 * @param mode The fopen mode
 *
 * @return a FILE or exit on failure
 */
void* fopen_wrapper(char *path, char *mode);

/**
 * malloc wrapper
 *
 * @param size The ammount of memory to be allocated
 *
 * @return A pointer to the allocated memory or exit on failure
 */
void* malloc_wrapper(size_t size);

/**
 * calloc wrapper
 *
 * @param number_items The number of items to be allocated
 * @param size The size of each item
 *
 * @return A pointer to the allocated memory or exit on failure
 */
void* calloc_wrapper(size_t number_items, size_t size);

/**
 * realloc wrapper
 *
 * @param pointer The pointer to the memory to be reallocated
 * @param number_items The number of items we need
 * @param size The size of each item
 *
 * @return Either a pointer to the new allocated memory or exit on failure
 */
void* realloc_wrapper(void* pointer, size_t number_items, size_t size);

/**
 * Computes the maximum of three values
 *
 * @param a First value
 * @param b Second value
 * @param c Third value
 *
 * @return The maximum of a, b and c
 */
unsigned long max3(unsigned long a, unsigned long b, unsigned long c);

/**
 * Turns the given string into an unsigned long
 *
 * @param string The string containing the original value
 * @param value The variable containing the data once turned into an unsigned
 * long
 *
 * @return NO_ERROR if there was no error or INVAL_ERROR if the given string
 * couldn't be turned into an unsigned long
 */
Error_enum strtoul_wrapper(char *string, unsigned long *value);

#endif

