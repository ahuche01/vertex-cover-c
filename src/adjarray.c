#include <stdio.h>
#include <time.h>
#include <string.h>

#include "utils.h"
#include "adjarray.h"

Error_enum read_line(char* string, unsigned long* s, unsigned long* t) {
		char* tmp = strtok(string, " \t\n");
		if (!tmp) {
			return FILE_FORMAT_ERROR;
		}

		if (*tmp == '#') {
			return COMMENT;
		}
		if(strtoul_wrapper(tmp, s)) {
			return FILE_FORMAT_ERROR;
		}

		tmp = strtok(NULL, " \t\n");
		if(strtoul_wrapper(tmp, t)) {
			return FILE_FORMAT_ERROR;
		}

		return NO_ERROR;
}

adjlist* readEdgeList(char* file_name) {
	FILE *file = fopen_wrapper(file_name, "r");
	unsigned long max_size = NBEDGES;
	adjlist *g = malloc_wrapper(sizeof(adjlist));
	char buffer[BUFSIZ];
	unsigned long line = 1;

	g->number_edges = 0;
	g->number_nodes = 0;

	g->edges = malloc(max_size*sizeof(edge));

	/* Can skip comments */
	while (fgets(buffer, BUFSIZ, file)) {
		Error_enum parsed = read_line(buffer, &g->edges[g->number_edges].s,
										&g->edges[g->number_edges].t);
		if (parsed == FILE_FORMAT_ERROR) {
			fprintf(stderr, "Error line %lu of %s\n", line, file_name);
			exit(EXIT_FAILURE);
		} else if (parsed == NO_ERROR) {
			g->number_nodes = max3(g->number_nodes,
									g->edges[g->number_edges].s,
									g->edges[g->number_edges].t);
			if (++(g->number_edges) == max_size) {
				max_size += NBEDGES;
				g->edges = realloc_wrapper(g->edges, max_size, sizeof(edge));
			}
		}
		line++;
	}

	fclose(file);

	g->edges = realloc_wrapper(g->edges, g->number_edges, sizeof(edge));

	return g;
}

void makeadjlist(adjlist* g) {
	/* Error from MaxDan94? Need to allocate one more than g->number_nodes
	* because we will check degrees[node_id]. node_id being g->number_node and
	* indices starting at 0, we need one more.
	*/
	unsigned long *degrees = calloc_wrapper(g->number_nodes+1,
											sizeof(unsigned long));
	unsigned long i, u, v;

	/* Building array of degrees. */
	for (i = 0; i < g->number_edges; i++) {
		degrees[g->edges[i].s]++;
		/* printf("Reading %lu, size %lu\n", g->edges[i].t, g->number_nodes); */
		degrees[g->edges[i].t]++;
	}

	/* Error from MaxDan94? Need to allocate number_nodes + 2 because the first
	* element is always 0, then we get number_nodes elements, then one more,
	* which is the index up until which the last node has neighbours in
	* g->neighbours.
	*/
	g->cumulative_degrees =
		malloc_wrapper((g->number_nodes+2)*sizeof(unsigned long));
	/* The first element is always 0 because that's where the neighbours of the
	* first node will always begin. It's just a padding. */
	g->cumulative_degrees[0] = 0;
	/* Building cumulative_degrees array. Each index represent a node, the
	* value represent the index of the array neighbours where its neighbours
	* begin.*/
	for (i = 1; i < g->number_nodes+2; i++) {
		g->cumulative_degrees[i] = g->cumulative_degrees[i-1] + degrees[i-1];
		degrees[i-1] = 0;
	}

	g->neighbours = malloc_wrapper(2*g->number_edges*sizeof(unsigned long));
	/* Building neighbours array. For each edge, take one end and store it as
	* neighbour of the other end */
	for (i = 0; i < g->number_edges; i++) {
		u = g->edges[i].s;
		v = g->edges[i].t;
		g->neighbours[g->cumulative_degrees[u] + degrees[u]++] = v;
		g->neighbours[g->cumulative_degrees[v] + degrees[v]++] = u;
	}

	free(degrees);
}

void free_adjlist(adjlist *g) {
	free(g->edges);
	free(g->cumulative_degrees);
	free(g->neighbours);
	free(g);
}

unsigned long get_degree(unsigned long node, adjlist* g) {
	if (node > g->number_nodes) {
		return 0;
	}
	return g->cumulative_degrees[node+1] - g->cumulative_degrees[node];
}

void print_degree(unsigned long node, adjlist* g) {
	printf("Node %lu has degree %lu.\n", node, get_degree(node, g));
}

void print_all_degrees(adjlist* g) {
	unsigned long i;
	for (i = 0; i < g->number_nodes+1; i++) {
		print_degree(i, g);
	}
}

void print_neighbours(unsigned node, adjlist* g) {
	unsigned long i;
	/* A node of degree 0 doesn't exist */
	if (get_degree(node, g)) {
		printf("%d has neighbours: ", node);
		for (i = g->cumulative_degrees[node];
				i < g->cumulative_degrees[node+1]; i++) {
			printf("%lu ",g->neighbours[i]);
		}
		printf("\n");
	}
}

void print_all_neighbours(adjlist* g) {
	unsigned long i;
	for (i = 0; i < g->number_nodes+1; i++) {
		print_neighbours(i, g);
	}
}

unsigned long has_neighbour_k(unsigned long node, unsigned long target,
							unsigned long k, adjlist* g) {
	unsigned long i;
	for (i = g->cumulative_degrees[node]+k; i < g->cumulative_degrees[node+1];
			i++) {
		if (g->neighbours[i] == target) {
			return i;
		}
	}
	return (unsigned long)-1;
}

neighbours_id_list* get_neighbours_2_bounds(unsigned long node, adjlist* g) {
	unsigned long i, nid_from = g->cumulative_degrees[node],
		nid_to = g->cumulative_degrees[node+1];
	neighbours_id_list* neighbours_list =
									malloc_wrapper(sizeof(neighbours_id_list));
	neighbours_list->n_list =
					malloc_wrapper(get_degree(node, g)*sizeof(neighbours_id));
	neighbours_list->length = get_degree(node, g);
	for (i = nid_from; i < nid_to; i++) {
		neighbours_list->n_list[i-nid_from].from = g->cumulative_degrees[g->neighbours[i]];
		neighbours_list->n_list[i-nid_from].to = g->cumulative_degrees[g->neighbours[i]+1];
	}
	return neighbours_list;
}

void free_neighbours_2(neighbours_id_list* neigh_list) {
	free(neigh_list->n_list);
	free(neigh_list);
}

void print_neighbours_2_(neighbours_id* nid, adjlist* g) {
	if (nid->to - nid->from) {
		unsigned long i;
		for (i = nid->from; i < nid->to; i++) {
			printf("%lu ", g->neighbours[i]);
		}
	}
}

char notin(unsigned long node, node_list* node_list) {
	unsigned long i;
	for (i = 0; i < node_list->length; i++) {
		if (node_list->list[i] == node) {
			return 0;
		}
	}
	return 1;
}

char not_neighbour(unsigned long target, unsigned long node, adjlist* g) {
	unsigned long i;
	/* A node of degree 0 doesn't exist */
	if (get_degree(node, g)) {
		for (i = g->cumulative_degrees[node];
				i < g->cumulative_degrees[node+1]; i++) {
			if (target == g->neighbours[i]) {
				return 0;
			}
		}
	}
	return 1;
}

node_list* build_neigh_2_list(neighbours_id_list* n_list, adjlist* g,
								unsigned long node) {
	unsigned long i, j;
	node_list *res = malloc_wrapper(sizeof(node_list));
	res->list = malloc_wrapper(g->number_nodes*sizeof(unsigned long));
	res->length = 0;
	for (i = 0; i < n_list->length; i++) {
		for (j = n_list->n_list[i].from; j < n_list->n_list[i].to; j++) {
			if (notin(g->neighbours[j], res)
					&& g->neighbours[j] != node
					&& not_neighbour(g->neighbours[j], node, g)) {
				res->list[res->length++] = g->neighbours[j];
			}
		}
	}
	/* Shouldn't realloc a NULL pointer because of how my wrapper is made */
	if (res->length) {
		res->list = realloc_wrapper(res->list, res->length,
									sizeof(unsigned long));
	} else {
		free(res->list);
		res->list = NULL;
	}
	free_neighbours_2(n_list);
	return res;
}

void free_node_list(node_list* node_list) {
	free(node_list->list);
	free(node_list);
}

void print_neighbours_2_list(node_list* res) {
	unsigned long i;
	for (i=0; i<res->length; i++) {
		printf("%lu ", res->list[i]);
	}
	printf("\n");
}

node_list* get_neighbours_2_list(unsigned long node, adjlist* g) {
	return build_neigh_2_list(get_neighbours_2_bounds(node, g), g, node);
}

void print_neighbours_2(neighbours_id_list* neigh_list, adjlist* g) {
	unsigned long i;
	for (i = 0; i < neigh_list->length; i++) {
		print_neighbours_2_(&neigh_list->n_list[i], g);
	}
	printf("\n");
}

char not_in_graph(unsigned long node, adjlist* g) {
	/* If the node is bigger than the biggest node of the graph or if it has
	* degree zero */
	return (g->number_nodes < node || !get_degree(node,g)) ? 1 : 0;
}

char in_graph(unsigned long node, adjlist* g) {
	return !not_in_graph(node, g);
}

void update_cumulative_degrees(unsigned long node, unsigned long degree,
		adjlist* g) {
	unsigned long i, k = 0;
	/* Updating cumulative_degrees array */
	for (i = 0; i < g->number_nodes+1; i++) {
		/* If the current node is the node to be removed, make its degree 0:
		* that is set the cumulative degree of the next node to the cumulative
		* degree of the current node. */
		if (i == node) {
			/* Starting at the next index, the cumulative_degree array will
			* need to be slided by the degree */
			k += degree;
		}
		/* Else if the current node is a neighbour of the node to be removed,
		* - decrement its upper bound in the cumulative degree array along with
		* all the nodes that come after. */
		else if (has_neighbour_k(i, node, k, g) < (unsigned long)-1) {
			/* Make the degree of the neighbour one less. */
			k++;
		}
		if (k) {
			/* Decrease the cumulative_degrees by one more for the rest of
			* the array */
			g->cumulative_degrees[i+1] -= k;
		}
	}
	/* Updating cumulative_degrees done */
}

void update_neighbours(unsigned long node, unsigned long degree,
		unsigned long cumulative_degrees_node, adjlist* g) {
	/* Updating neighbours array */
	/* Used as offset */
	unsigned long i, k = 0;
	/* Traverses the whole neighbours array. */
	for (i = 0; i < 2*g->number_edges; i++) {
		/* Make sure the offset k doesn't read out of bounds. */
		if (i < 2*g->number_edges-k) {
			/* If the offset is the first neighbour of the node to be removed,
			* skip degree neighbours. */
			if (i+k == cumulative_degrees_node) {
				k += degree;
			}
			/* If the offset is an outneighbour corresponding to the node to be
			* removed. */
			if (g->neighbours[i+k] == node && i < 2*g->number_edges-k-1) {
				k++;
				g->neighbours[i] = g->neighbours[i+k];
				/* Redo it in case the next neighbour is also to be deleted. */
				i--;
			} else {
				/* Slide neighbours as expected. */
				g->neighbours[i] = g->neighbours[i+k];
			}
		} else {
			/* Fill the rest of the array with 0s.
			* Is this really necessary? */
			g->neighbours[i] = 0;
		}
	}
	/* Updating neighbours array done */
}

void update_edges(unsigned long node, adjlist* g) {
	/* Updating edges array */
	unsigned long i, k = 0;
	/* Removes the edges that are adjacent to the node removed, from the edge
	* array.*/
	for (i = 0; i < g->number_edges; i++) {
		if (i+k < g->number_edges) {
			if (k) {
				g->edges[i] = g->edges[i+k];
			}
			if ((g->edges[i].s == node || g->edges[i].t == node)) {
				/* Slide the edges array in case the current edge is removed.
				* Redo it in case the next edge also contained the node to be
				* deleted. */
				k++, i--;
			}
		} else {
			/* Fill the rest of the array with (0,0) edges.
			* Is this really necessary? */
			edge e;
			e.s = 0, e.t = 0, g->edges[i] = e;
		}
	}
	/* Updating edges array done */
}

void update_number_nodes(adjlist* g) {
	unsigned long i;
	if (g->number_edges) {
		for (i = g->number_nodes; i > 0; i--) {
			if (get_degree(i, g)) {
				g->number_nodes = i, i = 1;
			}
		}
	} else {
		g->number_nodes = 0;
	}
}

void realloc_edges(adjlist* g) {
	if (g->number_edges) {
		g->edges = realloc_wrapper(g->edges, g->number_edges, sizeof(edge));
	} else {
		free(g->edges);
		g->edges = NULL;
	}
}

void realloc_neighbours(adjlist* g) {
	if (g->number_edges) {
		g->neighbours = realloc_wrapper(g->neighbours, 2*g->number_edges,
										sizeof(unsigned long));
	} else {
		free(g->neighbours);
		g->neighbours = NULL;
	}
}

void realloc_cumulative_degrees(adjlist* g) {
	g->cumulative_degrees = realloc_wrapper(g->cumulative_degrees,
											g->number_nodes+2,
											sizeof(unsigned long));
}

void rm_node(unsigned long node, adjlist* g) {
	unsigned long degree = get_degree(node, g),
		cumulative_degrees_node = g->cumulative_degrees[node];

	if (not_in_graph(node, g)) {
		fprintf(stderr, "Trying to remove node %lu which is not in the "
						"graph\n", node);
		/* exit(EXIT_FAILURE); */
	}

	/* Update cumulative_degrees before neighbours */
	update_cumulative_degrees(node, degree, g);
	update_neighbours(node, degree, cumulative_degrees_node, g);
	update_edges(node, g);

	/* Because of my wrappers, I can't return a NULL pointer so I need to check
	* for that. */
	g->number_edges -= degree;
	realloc_edges(g);
	realloc_neighbours(g);

	update_number_nodes(g);
	realloc_cumulative_degrees(g);
}

unsigned long get_number_edges(adjlist* g) {
	return g->number_edges;
}

unsigned long get_number_nodes(adjlist* g) {
	return g->number_nodes;
}

unsigned long get_neighbour(unsigned long node, unsigned long i, adjlist* g) {
	return g->neighbours[g->cumulative_degrees[node]+i];
}

edge get_edge(unsigned long i, adjlist* g) {
	return g->edges[i];
}

