#include <stdio.h>

#include "adjarray.h"
#include "utils.h"
#include "triangles.h"

char disjoint_triangle(triangles_list *t_list, triangle *tmp_triangle) {
	unsigned long i;
	for (i = 0; i < t_list->length; i++) {
		if (tmp_triangle->a == t_list->triangles[i].a
				|| tmp_triangle->a == t_list->triangles[i].b
				|| tmp_triangle->a == t_list->triangles[i].c
				|| tmp_triangle->b == t_list->triangles[i].a
				|| tmp_triangle->b == t_list->triangles[i].b
				|| tmp_triangle->b == t_list->triangles[i].c
				|| tmp_triangle->c == t_list->triangles[i].a
				|| tmp_triangle->c == t_list->triangles[i].b
				|| tmp_triangle->c == t_list->triangles[i].c
			) {
			/*
			printf("Found duplicate %lu %lu %lu\n", t_list->triangles[i].a,
													t_list->triangles[i].b,
													t_list->triangles[i].c);
			*/
			return 0;
		}
	}
	return 1;
}

void add_disjoint_triangle(triangles_list *t_list, triangle* tmp_triangle) {
	if (disjoint_triangle (t_list, tmp_triangle)) {
		t_list->triangles[t_list->length].a = tmp_triangle->a;
		t_list->triangles[t_list->length].b = tmp_triangle->b;
		t_list->triangles[t_list->length++].c = tmp_triangle->c;
	}
}

void free_triangles_list(triangles_list *t_list) {
	free(t_list->triangles);
	free(t_list);
}

triangles_list* find_triangles_naive(adjlist *g) {
	unsigned long i;
	triangles_list *t = malloc_wrapper(sizeof(triangles_list));

	t->triangles = malloc_wrapper(get_number_nodes(g)*sizeof(triangle));
	t->length = 0;

	for (i = 0; i < get_number_edges(g); i++) {
		/* Get each end of edge i. */
		unsigned long l, k, vertex_u = get_edge(i, g).s,
							vertex_v = get_edge(i, g).t;

		for (l = 0; l < get_degree(vertex_u, g); l++) {
			unsigned long neigh_u = get_neighbour(vertex_u, l, g);
			for (k = 0; k < get_degree(vertex_v, g); k++) {
				unsigned long neigh_v = get_neighbour(vertex_v, k, g);
				if(neigh_u == neigh_v) {
					triangle tmp_triangle;
					/* printf("Got triangle %lu %lu %lu.\n", g->neighbours[l],
								vertex_v, vertex_u); */
					tmp_triangle.a = neigh_v;
					tmp_triangle.b = vertex_v;
					tmp_triangle.c = vertex_u;
					/* Adds the current triangle only if disjoint from the
					* other triangles. */
					add_disjoint_triangle(t, &tmp_triangle);
				}
			}
		}
		if (i % 1000 == 0) {
			printf("Find triangle: %lu out of %lu edges processed\n", i,
					get_number_edges(g));
		}
	}

	/* If there are no triangles in the graph, handle it manually */
	if (t->length) {
		t->triangles = realloc_wrapper(t->triangles, t->length,
										sizeof(triangle));
	} else {
		free(t->triangles);
		t->triangles = NULL;
	}

	return t;
}

/* Old implementation of a naive triangle finding algorithm. Depends on the
 * adjlist datastructure, prefer the generic version */
triangles_list* find_triangles_naive_adjlist(adjlist *g) {
	triangles_list *t;
	unsigned long i;

	t = malloc_wrapper(sizeof(triangles_list));
	t->triangles = malloc_wrapper(get_number_nodes(g)*sizeof(triangle));
	t->length = 0;

	for (i = 0; i < get_number_edges(g); i++) {
		unsigned long vertex_u, vertex_v, l, k, uid_from, uid_to, vid_from, vid_to;
		/*neighbours_id *u_id, *v_id;*/

		/* Get each end of edge i. */
		vertex_u = g->edges[i].s;
		vertex_v = g->edges[i].t;

		/* Get neighbours bounds of both ends of the edge. */
		uid_from = g->cumulative_degrees[vertex_u],
		uid_to = g->cumulative_degrees[vertex_u+1],
		vid_from = g->cumulative_degrees[vertex_v],
		vid_to = g->cumulative_degrees[vertex_v+1];
		/*
		u_id = get_neighbours(vertex_u, g);
		v_id = get_neighbours(vertex_v, g);
		*/

		/* Check if there are common neighbours to both ends of the edge */
		for (l = uid_from; l < uid_to; l++) {
			for (k = vid_from; k < vid_to; k++) {
				if (g->neighbours[l] == g->neighbours[k]) {
					triangle tmp_triangle;
					/* printf("Got triangle %lu %lu %lu.\n", g->neighbours[l],
								vertex_v, vertex_u); */
					tmp_triangle.a = g->neighbours[l];
					tmp_triangle.b = vertex_v;
					tmp_triangle.c = vertex_u;
					/* Adds the current triangle only if disjoint from the
					* other triangles. */
					add_disjoint_triangle(t, &tmp_triangle);
				}
			}
		}
		/*
		free(v_id);
		free(u_id);
		*/
		if (i % 1000 == 0) {
			printf("%lu out of %lu\n", i, get_number_edges(g));
		}
	}

	t->triangles = realloc_wrapper(t->triangles, t->length, sizeof(triangle));
	return t;
}

void print_triangle(triangle t) {
	printf("(%lu, %lu, %lu)\n", t.a, t.b, t.c);
}

void print_all_triangles(triangles_list *t) {
	unsigned long i;
	printf("Triangles:\n");
	for (i = 0; i < t->length-1; i++) {
		print_triangle(t->triangles[i]);
	}
}

