/**
 * @file priority_queue.h
 * Priority queue implemented as a Heap, implemented as an array of (nodeid,
 * degree), ordered by degree
 */

#ifndef __PRIORITY_QUEUE__
#define __PRIORITY_QUEUE__

#include "adjarray.h"

/**
 * Structure storing a node with its degree
 */
typedef struct {
	/** The node */
	unsigned long node;
	/** Its degree */
	unsigned long degree;
} node_degree;

/**
 * A list of node and its degree, along with the length of that list
 */
typedef struct {
	/** The list of node and degree */
	node_degree* degree_list;
	/** The length of the list */
	unsigned long length;
} node_degree_list;

/**
 * Initializes a priority queue
 *
 * @param g The graph we are working on
 *
 * @return A priority queue allocated but empty
 */
node_degree_list* initialize_priority_queue(adjlist* g);

/**
 * Frees a priority queue
 *
 * @param deg_l The priority queue to be freed
 *
 * @return void
 */
void free_degree_list(node_degree_list* deg_l);

/**
 * Returns an empty node_degree
 *
 * @return an empty node_degree
 */
node_degree empty_node_degree();

/**
 * Returns the left child of a given node
 *
 * @param deg_l The priority queue we are working on
 * @param i The index of the given node
 *
 * @return The left child of the node at index i in deg_l
 */
node_degree get_left_child(node_degree_list* deg_l, unsigned long i);

/**
 * Returns the right child of a given node
 *
 * @param deg_l The priority queue we are working on
 * @param i The index of the given node
 *
 * @return The right child of the node at index i in deg_l
 */
node_degree get_right_child(node_degree_list* deg_l, unsigned long i);

/**
 * Swaps two elements of a priority queue
 *
 * @param deg_l The priority queue we are working on
 * @param i The first element to be swapped
 * @param j The second element to be swapped
 *
 * @return void
 */
void swap(node_degree_list* deg_l, unsigned long i, unsigned long j);

/**
 * Swaps a node with its parent
 *
 * @param deg_l The priority queue we are working on
 * @param i The node that should be swapped with its parent
 *
 * @return void
 */
void swap_parent(node_degree_list* deg_l, unsigned long i);

/**
 * Gets the parent of a given node
 *
 * @param deg_l The priority queue we are working on
 * @param i The element whose parent will be returned
 *
 * @return The parent of node at index i
 */
node_degree get_parent(node_degree_list* deg_l, unsigned long i);

/**
 * Checks if the node at index i is the root
 *
 * @param i The index of the node
 *
 * @return 1 if it has a parent (not the root), 0 otherwise
 */
char has_parent(unsigned long i);

/**
 * Balances the priority queue (smallest element at root, each element isn't
 * greater than its parents), after a new element is added at the end
 *
 * @param deg_l The priority queue we are working on
 * @param i The index of the new element
 *
 * @return void
 */
void balance(node_degree_list* deg_l, unsigned long i);

/**
 * Checks that the priority queue is consistent (each element isn't greater
 * than its parents)
 *
 * @param deg_l The priority queue we are working on
 *
 * @return 1 if consisdent, 0 otherwise
 */
char check_priority_queue_consistency (node_degree_list* deg_l);

/**
 * Adds a node to the priority queue
 *
 * @param node The node to be added
 * @param degree The degree of the node to be added
 * @param deg_l The priority queue we are working on
 *
 * @return void
 */
void add_node_pqueue(unsigned long node, unsigned long degree, node_degree_list* deg_l);

/**
 * Populate the priority queue with all nodes in the graph
 *
 * @param g The graph whose nodes will populate the priority queue
 * @param deg_list The priority queue that will be populated
 *
 * @return void
 */
void populate_priority_queue(adjlist* g, node_degree_list* deg_list);

/**
 * Prints the priority queue
 *
 * @param deg_l The priority queue that will be printed
 *
 * @return void
 */
void print_priority_queue(node_degree_list* deg_l);

/**
 * Gets and removed the minimum element of a priority queue
 *
 * @param deg_list The priority queue we are working on
 *
 * @return The node that has the minimum degree in the priority queue
 */
node_degree get_min_element(node_degree_list* deg_list);

#endif

